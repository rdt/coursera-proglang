use "9.sml";

fun month_range(day1 : int, day2 : int) =
  let
    fun month_range_rec (n : int, xs : int list) =
      if n > day2 then
        xs
      else
        month_range_rec(n + 1, xs @ [what_month n])
  in
    if day1 > day2 then
      []
    else
      month_range_rec(day1, [])
  end
