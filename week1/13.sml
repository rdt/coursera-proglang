fun reasonable_date (date : (int * int * int)) =
  let
    val year   = #1 date
    val month  = #2 date
    val day    = #3 date
    val months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    fun is_leap_year (n : int) =
      n mod 400 = 0 orelse (n mod 4 = 0 andalso n mod 100 <> 0)

    fun get_nth (xs : int list, n : int) =
      let
        fun get_nth_rec (xs : int list, counter : int) =
          if counter = n then
            hd xs
          else
            get_nth_rec(tl xs, counter + 1)
      in
        get_nth_rec(xs, 1)
      end

    fun days_in_month (n: int) =
      get_nth(months, n)

  in
    if year <= 0 then
      false
    else if month < 1 orelse month > 12 then
      false
    else if is_leap_year(year) andalso month = 2 andalso day = 29 then
      true
    else if day <= 0 orelse day > days_in_month(month) then
      false
    else
      true
  end
