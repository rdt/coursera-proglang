fun number_in_month (dates : (int * int * int) list, month : int) =
  let
    fun number_in_month_rec (xs : (int * int * int) list, i : int ) =
      if null xs then
        i
      else
        if #2 (hd xs) = month then
          number_in_month_rec(tl xs, i + 1)
        else
          number_in_month_rec(tl xs, i)
  in
    number_in_month_rec(dates, 0)
  end
