fun get_nth (s : string list, n : int) =
  let
    fun get_nth_rec (xs : string list, i : int) =
      if i = n then
        hd xs
      else
        get_nth_rec(tl xs, i + 1)
  in
    get_nth_rec(s, 1)
  end
