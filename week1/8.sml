fun number_before_reaching_sum (sum : int, numbers : int list) =
  let
    fun number_before_reaching_sum_recur (xs : int, ys : int list, i : int) =
      if xs >= sum then
        i
      else
        number_before_reaching_sum_recur(xs + hd ys, tl ys, i + 1)
  in
    number_before_reaching_sum_recur(hd numbers, tl numbers, 0)
  end
