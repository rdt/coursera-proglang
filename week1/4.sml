fun dates_in_month (dates : (int * int * int) list, month: int) =
  let
    fun dates_in_month_rec (xs : (int * int * int) list, ys : (int * int * int) list) =
      if null xs then
        ys
      else
        if #2 (hd xs) = month then
          dates_in_month_rec(tl xs, ys @ [hd xs])
        else
          dates_in_month_rec(tl xs, ys)
  in
    dates_in_month_rec(dates, [])
  end
