use "1.sml";
use "test-helper.sml";

test "#is_older" [
  is_older((2003, 12, 30), (2004, 12, 12)) = true,
  is_older((2004, 12, 10), (2004, 13, 30)) = true,
  is_older((2004, 12, 10), (2004, 12, 30)) = true,
  is_older((2004, 12, 22), (2004, 12, 22)) = false
]
