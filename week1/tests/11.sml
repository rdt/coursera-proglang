use "11.sml";
use "test-helper.sml";

test "#oldest" [
  let
    val expr1 = oldest [(2001, 12, 30), (2003, 10, 10), (2000, 12, 12)]
  in
    valOf expr1 = (2000, 12, 12)
  end ,

  let
    val expr2 = oldest
      [(2001, 12, 30), (1998, 10, 10), (2000, 12, 12), (1998, 11, 10)]
  in
    valOf expr2 = (1998, 10, 10)
  end ,

  let
    val expr3 = oldest []
  in
    isSome expr3 = false
  end
]
