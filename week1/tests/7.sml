use "7.sml";
use "test-helper.sml";

test "#date_to_string" [
  date_to_string(1988, 11, 21) = "November 21, 1988",
  date_to_string(2001, 10, 11) = "October 11, 2001"
]
