use "8.sml";
use "test-helper.sml";

test "#number_before_reaching_sum" [
  number_before_reaching_sum(1, [2]) = 0,
  number_before_reaching_sum(5, [3, 2, 2]) = 1,
  number_before_reaching_sum(4, [1, 4, 1, 1]) = 1,
  number_before_reaching_sum(6, [4, 1, 1, 1]) = 2,
  number_before_reaching_sum(10, [1, 2, 3, 4, 5]) = 3,
  number_before_reaching_sum(5, [1, 3, 0, 0, 2, 10]) = 4
]
