use "10.sml";
use "test-helper.sml";

test "#month_range" [
  month_range(31, 32) = [1, 2],
  month_range(3, 3) = [1],
  month_range(40, 18) = []
]
