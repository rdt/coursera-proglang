use "13.sml";
use "test-helper.sml";

test "#reasonable_date" [
  reasonable_date (2000, 10, 10) = true,
  reasonable_date (0, 10, 10) = false,
  reasonable_date (2003, 1, 32) = false,
  reasonable_date (2003, 13, 20) = false,
  reasonable_date (2000, 2, 29) = true,
  reasonable_date (2001, 2, 29) = false
]
