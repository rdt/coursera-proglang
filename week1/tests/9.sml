use "9.sml";
use "test-helper.sml";

test "#what_month" [
  what_month(20) = 1,
  what_month(40) = 2,
  what_month(350) = 12
]
