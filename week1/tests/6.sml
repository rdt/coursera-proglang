use "6.sml";
use "test-helper.sml";

test "#get_nth" [
  get_nth(["foo", "bar", "baz", "quux"], 1) = "foo",
  get_nth(["foo", "bar", "baz", "quux"], 2) = "bar",
  get_nth(["foo", "bar", "baz", "quux"], 4) = "quux",
  get_nth(["foo", "bar", "quux"], 2) = "bar"
]
