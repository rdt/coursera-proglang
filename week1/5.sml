use "4.sml";

fun dates_in_months (dates: (int * int * int) list, months : int list) =
  let
    fun dates_in_months_rec (xs : int list, ys : (int * int * int ) list) =
      if null xs then
        ys
      else
        dates_in_months_rec(tl xs, ys @ dates_in_month(dates, hd xs))
  in
    dates_in_months_rec(months, [])
  end
