use "8.sml";

fun what_month (n : int) =
  1 + number_before_reaching_sum(n, [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31])
