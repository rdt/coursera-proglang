use "3.sml";
use "5.sml";

fun remove_duplicates (lst : int list) =
  let
    fun item_in_list (item : int, zs : int list) =
      if null zs then
        false
      else if item = hd zs then
        true
      else
        item_in_list(item, tl zs)

    fun remove_duplicates_rec (xs : int list, ys : int list) =
      if null xs then
        ys
      else if item_in_list(hd xs, ys) then
        remove_duplicates_rec(tl xs, ys)
      else
        remove_duplicates_rec(tl xs, ys @ [hd xs])
  in
    if null lst then
      []
    else
      remove_duplicates_rec(tl lst, [hd lst])
  end

fun number_in_months_challenge (dates : (int * int * int) list, months : int list) =
  number_in_months(dates, remove_duplicates(months))

fun dates_in_months_challenge (dates : (int * int * int) list, months : int list) =
  dates_in_months(dates, remove_duplicates(months))
