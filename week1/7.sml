use "6.sml";

fun date_to_string (date: int * int * int) =
  let
    val year   = #1 date
    val month  = #2 date
    val day    = #3 date
    val months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
  in
    get_nth(months, month) ^ " " ^ Int.toString(day) ^ ", " ^ Int.toString(year)
  end
