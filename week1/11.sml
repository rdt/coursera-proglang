use "1.sml";

fun oldest (dates : (int * int * int) list) =
  let
    fun oldest_rec (xs : (int * int * int) list, ys : (int * int * int)) =
      if null xs then
        ys
      else
        if is_older(ys, hd xs) then
          oldest_rec(tl xs, ys)
        else
          oldest_rec(tl xs, hd xs)
  in
    if null dates then
      NONE
    else
      SOME(oldest_rec(tl dates, hd dates))
  end
