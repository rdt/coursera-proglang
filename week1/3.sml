use "2.sml";

fun number_in_months (dates : (int * int * int) list, months : int list) =
  let
    fun number_in_months_rec(xs: int list, i : int) =
      if null xs then
        i
      else
        number_in_months_rec(tl xs, i + number_in_month(dates, hd xs))
  in
    number_in_months_rec(months, 0)
  end
