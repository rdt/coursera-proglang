(* Author: Ferruccio Barletta
  https://class.coursera.org/proglang-2012-001/forum/thread?thread_id=233 *)

fun test name tests =
  let
    fun test' t acc =
      (print "  #"; print (Int.toString(acc));
      print (if t then " PASSED\n" else " FAILED\n"); acc);
  in
    print "\ntesting "; print name; print "...\n";
    foldl (fn (t,acc) => (test' t (acc + 1))) 0 tests
  end
