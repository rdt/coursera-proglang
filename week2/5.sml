use "hw2provided.sml";

fun card_color(suit, rank) =
  case suit of
       Spades   => Black
     | Clubs    => Black
     | Diamonds => Red
     | Hearts   => Red
