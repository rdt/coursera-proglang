use "hw2provided.sml";

fun remove_card(cs, c, e) =
  case cs of
       []    => raise e
     | x::xs => case x = c of
                     true  => xs
                   | false => [x] @ remove_card(xs, c, e)
