use "2.sml";

fun similar_names(xs, {first=f, last=l, middle=m}) =
  let
    fun similar_names_rec([], acc) = acc
      | similar_names_rec(y::ys, acc) =
        similar_names_rec(ys, acc @ [{first=y, last=l, middle=m}])
  in
    similar_names_rec(get_substitutions1(xs, f), []) @ [{first=f, last=l, middle=m}]
  end
