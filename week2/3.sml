use "1.sml";

fun get_substitutions2(xs, s) =
  let
    fun get_substitutions2_rec([], acc) = acc
      | get_substitutions2_rec(y::ys, acc) =
        case all_except_option(s, y) of
             NONE   => get_substitutions2_rec(ys, acc)
           | SOME z => get_substitutions2_rec(ys, acc @ z)
  in
    get_substitutions2_rec(xs, [])
  end
