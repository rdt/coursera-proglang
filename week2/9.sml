use "hw2provided.sml";
use "6.sml";

fun sum_cards(xs) =
  let
    fun sum_cards_rec([], acc) = acc
      | sum_cards_rec(y::ys, acc) =
        sum_cards_rec(ys, acc + card_value(y))
  in
    sum_cards_rec(xs, 0)
  end
