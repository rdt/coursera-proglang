use "hw2provided.sml";

fun all_except_option (str, []) = NONE
  | all_except_option (str, x::xs) =
  case (same_string(str, x), all_except_option(str, xs)) of
       (true, SOME ys)  => SOME ys
     | (true, NONE)     => SOME xs
     | (false, SOME ys) => SOME(x::ys)
     | (false, NONE)    => NONE
