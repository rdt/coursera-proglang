use "1.sml";

fun get_substitutions1([], s) = []
  | get_substitutions1(x::xs, s) =
    case all_except_option(s, x) of
         NONE   => get_substitutions1(xs, s)
       | SOME y => y @ get_substitutions1(xs, s)
