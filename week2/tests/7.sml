use "7.sml";
use "test-helper.sml";

exception TestException

val c1 = (Spades, Ace);
val c2 = (Spades, King);
val c3 = (Spades, Queen);
val c4 = (Clubs, King);

test "#remove_card" [
  remove_card([c1, c2, c3], c2, TestException) = [c1, c3],
  remove_card([c1, c2, c3, c2], c2, TestException) = [c1, c3, c2],
  remove_card([c1, c3, c3, c2], c3, TestException) = [c1, c3, c2],

  (remove_card([c1, c2], c4, TestException)
    handle TestException => [c4]) = [c4]
]
