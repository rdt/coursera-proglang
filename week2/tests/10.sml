use "10.sml";
use "test-helper.sml";

val c1 = (Spades, King);
val c2 = (Spades, Ace);
val c3 = (Hearts, Ace);

test "#score" [
  score([],           11) = 5,
  score([c2],         12) = 1,
  score([c2],         11) = 0,
  score([c2],         10) = 0,
  score([c2, c1],     11) = 0,
  score([c2, c1],     21) = 0,
  score([c2, c1],     22) = 0,
  score([c2, c1, c3], 22) = 0,
  score([c2, c1, c3], 32) = 0,
  score([c2, c1, c3], 42) = 10
];
