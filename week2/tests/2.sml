use "2.sml";
use "test-helper.sml";

test "#get_substitutions1" [
  get_substitutions1(
    [["Fred","Fredrick"],["Elizabeth","Betty"],["Freddie","Fred","F"]],
    "Fred") =
    ["Fredrick","Freddie","F"],

  get_substitutions1(
    [["Fred","Fredrick"],["Jeff","Jeffrey"],["Geoff","Jeff","Jeffrey"]],
    "Jeff") =
    ["Jeffrey","Geoff","Jeffrey"],

  get_substitutions1(
    [["Fred","Fredrick"],["Jeff","Jeffrey"],["Geoff","Jeff","Jeffrey"]],
    "Matt") =
    []
]
