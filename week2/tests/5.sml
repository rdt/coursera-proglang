use "5.sml";
use "test-helper.sml";

test "#card_color" [
  card_color(Clubs, Jack)   = Black,
  card_color(Hearts, Queen) = Red,
  card_color(Spades, Queen) = Black
];
