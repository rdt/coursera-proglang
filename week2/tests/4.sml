use "4.sml";
use "test-helper.sml";

test "#similar_names" [
  similar_names(
    [["Fred","Fredrick"],["Elizabeth","Betty"],["Freddie","Fred","F"]],
    {first="Fred", middle="W", last="Smith"}) =

    [{first="Fredrick", last="Smith", middle="W"},
    {first="Freddie",   last="Smith", middle="W"},
    {first="F",         last="Smith", middle="W"},
    {first="Fred",      last="Smith", middle="W"}],

  similar_names(
    [],
    {first="Freed", middle="W", last="Smith"}) =
    [{first="Freed", middle="W", last="Smith"}],

  similar_names(
    [["Elizabeth", "Betty"]],
    {first="Freed", middle="W", last="Smith"}) =
    [{first="Freed", middle="W", last="Smith"}]
]
