use "3.sml";
use "test-helper.sml";

test "#get_substitutions2" [
  get_substitutions2(
    [["Fred","Fredrick"],["Elizabeth","Betty"],["Freddie","Fred","F"]],
    "Fred") =
    ["Fredrick","Freddie","F"],

  get_substitutions2(
    [["Fred","Fredrick"],["Jeff","Jeffrey"],["Geoff","Jeff","Jeffrey"]],
    "Jeff") =
    ["Jeffrey","Geoff","Jeffrey"],

  get_substitutions2(
    [["Fred","Fredrick"],["Jeff","Jeffrey"],["Geoff","Jeff","Jeffrey"]],
    "Matt") =
    []
]
