use "1.sml";
use "test-helper.sml";

test "#all_except_option" [
  all_except_option("foo", []) = NONE,
  all_except_option("foo", ["foo", "baz"]) = SOME ["baz"],
  all_except_option("foo", ["foo", "baz"]) = SOME ["baz"],
  all_except_option("foo", ["baz", "bar", "quux"]) = NONE,
  all_except_option("foo", ["baz", "bar", "quux"]) = NONE,
  all_except_option("foo", ["foo", "baz", "foo", "quux"]) = SOME ["baz", "quux"]
];
