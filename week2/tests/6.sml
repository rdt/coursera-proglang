use "6.sml";
use "test-helper.sml";

test "#card_value" [
  card_value(Hearts, Ace) = 11,
  card_value(Hearts, Jack) = 10,
  card_value(Hearts, Num 10) = 10,
  card_value(Hearts, Num 6) = 6
];
