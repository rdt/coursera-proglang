use "8.sml";
use "test-helper.sml";

val c1 = (Spades, Ace);
val c2 = (Clubs, King);
val c3 = (Spades, Queen);
val c4 = (Clubs, King);
val c5 = (Hearts, King);

test "#all_same_color" [
  all_same_color([c1, c2, c3]) = true,
  all_same_color([c1, c2]) = true,
  all_same_color([c1, c2, c4]) = true,
  all_same_color([c1, c5]) = false,
  all_same_color([c1]) = true
]
