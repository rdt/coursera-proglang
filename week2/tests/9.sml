use "9.sml";
use "test-helper.sml";

val c1 = (Spades, Ace);
val c2 = (Spades, King);
val c3 = (Spades, Queen);
val c4 = (Clubs, King);
val c5 = (Clubs, Num 6);
val c6 = (Clubs, Num 10);

test "#sum_cards" [
  sum_cards([c5, c6]) = 16,
  sum_cards([c1, c6]) = 21,
  sum_cards([c1, c2, c3]) = 31,
  sum_cards([c4]) = 10
]
