use "hw2provided.sml";

fun card_value(suit, rank) =
  case rank of
       Num i => i
     | Ace   => 11
     | _     => 10
