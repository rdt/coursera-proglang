use "5.sml";

fun all_same_color(cs) =
  case cs of
       []       => true
     | x::[]    => true
     | x::y::xs => (card_color x = card_color y) andalso all_same_color(y::xs)
