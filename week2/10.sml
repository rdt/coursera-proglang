use "8.sml";
use "9.sml";

fun preliminary_score(cs, goal) =
  let
    val sum = sum_cards cs
  in
    if sum > goal then
      3 * sum - goal
    else
      goal - sum
  end

fun score(cs, goal) =
  let
    val ps = preliminary_score(cs, goal);
  in
    if all_same_color cs then
      ps div 2
    else
      ps
  end
