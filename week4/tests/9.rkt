#lang racket

(require rackunit
         "../9.rkt")

(check-equal? (vector-assoc 1 (vector)) #f)
(check-equal? (vector-assoc 1 (vector  1 2 3 4 5)) #f)
(check-equal? (vector-assoc 4 (vector  1 2 3 (cons 3 "foo") 5)) #f)
(check-equal? (vector-assoc 4 (vector  1 2 3 (cons 4 "foo") 5)) '(4 . "foo"))
