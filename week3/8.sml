fun all_answers f [] = SOME []
  | all_answers f xs =
  let
    fun all_answers_rec ([], acc) = acc
      | all_answers_rec (y::ys, acc) =
      case f y of
           SOME v => all_answers_rec(ys, acc @ [v])
         | NONE   => all_answers_rec(ys, acc)
  in
    case all_answers_rec(xs, []) of
         [] => NONE
       | xs => SOME xs
  end
