val longest_string2 =
  foldl (fn (x, y) => if String.size x >= String.size y then x else y) ""
