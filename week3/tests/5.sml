use "5.sml";
use "test-helper.sml";

test "#longest_capitalized" [
  longest_capitalized(["foo", "bar", "baz"]) = "",
  longest_capitalized(["foo", "Bar", "baz"]) = "Bar",
  longest_capitalized(["foo", "Bar", "Baz"]) = "Bar",
  longest_capitalized(["foo", "Bar", "Quux", "Bar"]) = "Quux"
]
