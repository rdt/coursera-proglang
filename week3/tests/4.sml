use "4.sml";
use "test-helper.sml";

test "#longest_string3" [
  longest_string3([]) = "",
  longest_string3(["foo", "bar", "baz"]) = "foo",
  longest_string3(["foo", "bar", "baz", "quux"]) = "quux"
];

test "#longest_string4" [
  longest_string4([]) = "",
  longest_string4(["foo", "bar", "baz"]) = "baz",
  longest_string4(["foo", "bar", "baz", "quux"]) = "quux"
]
