use "3.sml";
use "test-helper.sml";

test "#longest_string2" [
  longest_string2([]) = "",
  longest_string2(["foo", "bar", "baz"]) = "baz",
  longest_string2(["foo", "bar", "baz", "quux"]) = "quux"
]
