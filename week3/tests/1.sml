use "1.sml";
use "test-helper.sml";

test "#only_capitals" [
  only_capitals(["foo"]) = [],
  only_capitals(["Foo"]) = ["Foo"],
  only_capitals(["foo", "Bar"]) = ["Bar"],
  only_capitals(["Foo", "Bar"]) = ["Foo", "Bar"],
  only_capitals(["Foo", "bar", "Baz"]) = ["Foo", "Baz"]
]
