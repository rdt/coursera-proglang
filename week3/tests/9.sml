use "9.sml";
use "test-helper.sml";

test "#count_wildcards" [
  count_wildcards(Wildcard) = 1,
  count_wildcards(TupleP [Wildcard, ConstP 42])  = 1,
  count_wildcards(TupleP [Wildcard, Wildcard])   = 2,
  count_wildcards(ConstructorP("foo", Wildcard)) = 1,
  count_wildcards(ConstructorP("foo", TupleP [Wildcard, Wildcard])) = 2
];

test "#count_wild_and_variable_lengths" [
  count_wild_and_variable_lengths(Wildcard) = 1,
  count_wild_and_variable_lengths(TupleP [Wildcard]) = 1,
  count_wild_and_variable_lengths(TupleP [Wildcard, Wildcard]) = 2,
  count_wild_and_variable_lengths(TupleP [Wildcard, Variable "quux"]) = 5
];

test "#count_some_var" [
  count_some_var("x", Wildcard) = 0,
  count_some_var("x", ConstP 42) = 0,
  count_some_var("x", Variable "x") = 1,
  count_some_var("x", TupleP [Variable "x", Variable "x"]) = 2,
  count_some_var("x", ConstructorP("foo", TupleP [Variable "x"])) = 1
]
