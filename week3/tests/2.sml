use "2.sml";
use "test-helper.sml";

test "#longest_string1" [
  longest_string1([]) = "",
  longest_string1(["foo", "bar", "baz"]) = "foo",
  longest_string1(["foo", "bar", "baz", "quux"]) = "quux"
]
