use "8.sml";
use "test-helper.sml";

fun p x =
  if (x mod 2) = 0 then
    SOME x
  else
    NONE;

test "#all_answers" [
  all_answers p []      = SOME [],
  all_answers p [42]    = SOME [42],
  all_answers p [1, 42] = SOME [42],
  all_answers p [1, 3]  = NONE,
  all_answers p [2, 42] = SOME [2, 42]
]
