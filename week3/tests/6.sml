use "6.sml";
use "test-helper.sml";

test "#rev_string" [
  rev_string "foo" = "oof",
  rev_string "xuuq" = "quux",
  rev_string "" = ""
]
