use "7.sml";
use "test-helper.sml";

fun p x =
  if (x mod 2) = 0 then
    SOME x
  else
    NONE;

test "#first_answer" [
  (first_answer p [] handle NoAnswer => 42) = 42,
  first_answer p [42] = 42,
  first_answer p [1, 42] = 42,
  (first_answer p [1, 3] handle NoAnswer => 42) = 42,
  first_answer p [2, 42] = 2
]
