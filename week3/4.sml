use "2.sml";
use "3.sml";

fun longest_string_helper f =
  foldl (fn (x, y) => if f(String.size x, String.size y) then x else y) ""

val longest_string3 = longest_string_helper op >
val longest_string4 = longest_string_helper op >=
