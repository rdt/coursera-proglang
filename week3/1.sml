use "hw3provided.sml";

val only_capitals =
  List.filter (fn s => Char.isUpper (String.sub(s, 0)))
