use "hw3provided.sml";

fun first_answer f xs =
  case xs of
       [] => raise NoAnswer
     | x::xs' => case f x of
                      SOME v => v
                    | NONE => first_answer f xs'
