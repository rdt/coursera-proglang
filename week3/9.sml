use "hw3provided.sml";

val count_wildcards =
  g (fn _ => 1) (fn _ => 0)

val count_wild_and_variable_lengths =
  g (fn _ => 1) (fn x => String.size x)

fun count_some_var (s, p) =
  g (fn _ => 0) (fn x => if x = s then 1 else 0) p
